# -*- coding: utf-8 -*-
"""
Created on Wed Jun  8 01:20:20 2016

@author: PEGO
"""


import pandas
from urllib.request import urlopen

from bs4 import BeautifulSoup

soup = BeautifulSoup(urlopen('http://www2.bmf.com.br/pages/portal/bmfbovespa/boletim1/TxRef1.asp'))

bmf=[]
for row in soup.select("table.tabConteudo"):
    for cell in row.find_all("td"):
        bmf.append(cell.get_text(strip=True))
        
di_futuro=[]
i=6
while i<len(bmf):
    di_futuro.append([bmf[i-2], bmf[i-1], bmf[i]])
    i=i+3
    
for valor in di_futuro:
    valor[0]=int(valor[0].replace(",", "."))
    valor[1]=float(valor[1].replace(",", "."))/100
    valor[2]=float(valor[2].replace(",", "."))/100


#completa a curva

di_futuro_full=[]
for i in range (1,1803):
    di_futuro_full.append([i,0,0])

for a in di_futuro_full:
    for b in di_futuro:
        if b[0]==a[0]:
            a[1]=b[1]
            a[2]=b[2]


#completa a curva c/ método flat forward
#para esse metodo temos que ter os pontos mais proximos anteriores e posteriores assim como a distancia me dia do ponto buscado

for i in range(1,len(di_futuro_full)-2): #
    #o primeiro valor é sempre o start, por isso iniciamos do segundo
    a=i
    p=i
    if di_futuro_full[i][2]==0.0:
        while di_futuro_full[a][2]==0.0:
            a -=1
            if a<=0:
                anterior=di_futuro[0]
                break
            else:
                anterior=di_futuro_full[a]
            
        while di_futuro_full[p][2]==0.0:
            p +=1
            posterior=di_futuro_full[p]
        #fazer a conta do flat forward
        termo=(((1+posterior[2])**(posterior[0]/360))/((1+anterior[2])**(anterior[0]/360)))**(360/(posterior[0]-anterior[0]))#calcula taxa a termo
        di_futuro_full[i][2]=(((1+anterior[2])**(anterior[0]/360)*termo**((di_futuro_full[i][0]-anterior[0])/360))**(360/di_futuro_full[i][0]))-1
        di_futuro_full[i][1]=di_futuro_full[i][2]
    else:
        anterior=0
        posterior=0
        
for i in range(len(di_futuro_full)+1,15000):
    info=[i,di_futuro_full[len(di_futuro_full)-2][1],di_futuro_full[len(di_futuro_full)-2][2]]
    di_futuro_full.append(info)
    
for i in range(0,180):
    print (di_futuro_full[i])
    
print (len(di_futuro_full))



with open("curva_juros.csv","w") as arquivo:
    for line in di_futuro_full:
        for row in line:
            arquivo.write(str(row) +";")
        arquivo.write("\n")


curva_csv=[]        
with open("curva_juros.csv","r") as leitura:
    for line in leitura.readlines():
        linha=line.strip().split(';')
        curva_csv.append(linha)
        
for i in range(len(curva_csv)):
    curva_csv[i][0]=int(curva_csv[i][0]) 
    curva_csv[i][1]=float(curva_csv[i][1])
    curva_csv[i][2]=float(curva_csv[i][2])
    del curva_csv[i][3]
    

            
            
    
        
    
    
    
        
        




