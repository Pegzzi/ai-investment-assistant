# -*- coding: utf-8 -*-
"""
Created on Thu Jun  9 00:13:39 2016

@author: PEGO
"""



#SELIC ATUAL e Regra para Poupança
selic_meta=14.25
def regra_tx_poup(s):
    if s>=8.5:
        return 0.005
    else:
        return s*0.7/100
#calculo TR no período

#IOF p cada dia de investimento até 30 dias corridos
tabela=[0, 0.96 , 0.93 , 0.90 ,0.86 ,0.83, 0.80, 0.76, 0.73, 0.70, 0.66, 0.63, 0.60, 0.56, 0.53, 0.50, 0.46, 0.43, 0.40, 0.36, 0.33, 0.30, 0.27, 0.23, 0.20,0.16, 0.13, 0.10, 0.06, 0.03, 0.00]

#limite maximo de dias e suas aliquotas de IR
IR=[[180,0.225],[360,0.200],[720,0.175],0.15]

#taxas pu base, criaremos funções para redefinir esses valores de acordo com a intenção de investimento do usuário
taxa_pu_ref={'%CDI CDB':1.00, 
          '%CDI LCI_LCA': 0.94 , 
          'TR': 0.0015
          }

LTN=[
        [2017,0,0],
        [2018,0,0],
        [2019,0.124,746.59],
        [2021,0,0],
        [2023,0.1261,462.55]]
        
NTNF=[
        ['NTNF',2017,0],
        ['NTNF',2012,0],
        ['NTNF',2023,0],
        ['NTNF',2025,0],
        ['NTNF',2027,0],
]

def cdi(juros,taxa): #calcula cdi over
    return (((((1+juros)**(1/float(360))-1)*taxa)+1)**360)-1
        
 
def aliquota_ir(dias):
    if dias<=IR[0][0]: #retorna a aliquota de IR (aliquota é diferente por produto)
        return IR[0][1]
    elif dias<=IR[1][0]:
        return IR[1][1]
    elif dias<=IR[2][0]:
        return IR[2][1]
    else:
        return IR[3]
#classe para cdb generico
        
        
        
        
     
     
     
     
     
from datetime import date, datetime, timedelta      


class Ponto_ponto(object):

    
    def __init__ (self,juros, dias,aporte,taxa):
        self.juros=juros
        self.dias=dias
        self.aporte=aporte
        self.taxa=taxa
    

 
    def cdb(self): # retorna rentabilidade do cdb
        cdi_over=cdi(self.juros,self.taxa)
        aliquota=aliquota_ir(self.dias)
        if self.dias<=30:
            return (((1+cdi_over)**(self.dias/float(360)))*self.aporte-self.aporte)*(1-tabela[self.dias])*(1-aliquota)+self.aporte
        else:
            return (((1+cdi_over)**(self.dias/float(360)))*self.aporte-self.aporte)*(1-aliquota)+self.aporte


    def lci_lca(self):# retorna rentabilidade do lci_lca
        cdi_over=cdi(self.juros,self.taxa)
        if self.dias<=30:
            return (((1+cdi_over)**(self.dias/float(360)))*self.aporte-self.aporte)*(1-tabela[self.dias])+self.aporte
        else:
            return (((1+cdi_over)**(self.dias/float(360)))*self.aporte-self.aporte)+self.aporte  
            
    def Tesouro_pre(self):#rentabilidade LFT (Letra pre fixada)
        vencimento=datetime.now()
        vencimento += timedelta(days=self.dias)
        if vencimento.month>1:
            titulo_min=vencimento.year+1
        else:
            titulo_min=vencimento.year
        
        for papel in LTN:    
            if papel[1]>0 and papel[0]>=titulo_min:
                titulo=papel
                break
            else:
                titulo=[]
        
        #qtde=int(self.aporte/int(titulo[2]*0.1))#calcula qtde maxima a ser comprada
        #return ((qtde*titulo[2]*0.1*((1+titulo[1])**(self.dias/360)-1)*(1-aliquota_ir(self.dias)))+self.aporte)#os juros da LTN são retiraos do site, e estao na base 252
        return ((self.aporte*((1+titulo[1])**(self.dias/360)-1)*(1-aliquota_ir(self.dias)))+self.aporte)
        
    def Tesouro_selic(self):
        return ((self.aporte*((1+selic_meta/100)**(self.dias/360)-1)*(1-aliquota_ir(self.dias)))+self.aporte)
        
    def poup(self):
        inv_day=datetime.now()
        inv_day += timedelta(days=self.dias) #adiciona o numero de dias do investimento
        
        if inv_day.month>=datetime.today().month:
            qtde_ant_aniv= (inv_day.month-datetime.today().month)+12*(inv_day.year-datetime.today().year)
        else:
            qtde_ant_aniv= 12*(inv_day.year-datetime.today().year)+(inv_day.month-datetime.today().month)#quantidade de meses VENCIDOS passados 
        if datetime.now().day>=29:#aniversarios no dia 29,30 e 31 vao para o dia 1
            aniversario= 1
        else:
            aniversario= datetime.now().day
            
        if inv_day.day >= aniversario:#aniversario ja aconteceu no mês atual
            return self.aporte*(1+taxa_pu_ref['TR']+regra_tx_poup(selic_meta))**(qtde_ant_aniv)
        elif inv_day.day <= aniversario:#aniversario ainda nao aconteceu no mes atual
            return self.aporte*(1+taxa_pu_ref['TR']+regra_tx_poup(selic_meta))**(qtde_ant_aniv-1)
        else:
            return self.aporte
                





class Aportes_periodicos(object):
    
    def __init__(self,produto, dias,periodicidade,aport_per,taxa):
        self.dias=dias        
        self.periodicidade=periodicidade
        self.aport_per=aport_per
        self.taxa=taxa
        self.produto=produto
        
    def looping(self):
        dt_aplicacao=self.periodicidade
        rentabilidade=0
        if self.produto=="CDB":
            while dt_aplicacao<self.dias:
                termo=(((1+di_futuro_full[self.dias][2])**(di_futuro_full[self.dias][0]/360))/((1+di_futuro_full[dt_aplicacao][2])**(di_futuro_full[dt_aplicacao][0]/360)))**(360/(di_futuro_full[self.dias][0]-di_futuro_full[dt_aplicacao][0]))
                iteracao=Ponto_ponto(termo-1,(self.dias-dt_aplicacao),self.aport_per,self.taxa)
                rentabilidade=rentabilidade+iteracao.cdb()
                dt_aplicacao += self.periodicidade
            return rentabilidade
        if self.produto=="POUPANCA":
            while dt_aplicacao<self.dias:
                iteracao=Ponto_ponto(0,(self.dias-dt_aplicacao),self.aport_per, self.taxa)
                rentabilidade = rentabilidade + iteracao.poup()
                dt_aplicacao += self.periodicidade
            return rentabilidade
        if self.produto=="LCI" or self.produto=="LCA":
            while dt_aplicacao<self.dias:
                termo=(((1+di_futuro_full[self.dias][2])**(di_futuro_full[self.dias][0]/360))/((1+di_futuro_full[dt_aplicacao][2])**(di_futuro_full[dt_aplicacao][0]/360)))**(360/(di_futuro_full[self.dias][0]-di_futuro_full[dt_aplicacao][0]))
                iteracao=Ponto_ponto(termo-1,(self.dias-dt_aplicacao),self.aport_per,self.taxa)
                rentabilidade=rentabilidade+iteracao.cdb()
                dt_aplicacao += self.periodicidade
            return rentabilidade
       
        

    
#cria objetos a partir das oplicações desejadas para simular

user_dict=[ { "Produto":"POUPANCA",
            "Taxa":0.82,
            "Aporte": 50000,
            "Prazo":365},


            { "Produto":"CDB",
            "Taxa":1.2,
            "Aporte": 50000,
            "Prazo":365},
            
            { "Produto":"LCI",
            "Taxa":0.94,
            "Aporte": 50000,
            "Prazo":365},
            
            { "Produto":"TESOURO-PRE",
            "Taxa":0.92,
            "Aporte": 50000,
            "Prazo":365},
            
            { "Produto":"TESOURO-SELIC",
            "Taxa":0.92,
            "Aporte": 50000,
            "Prazo":365}]
            
user_dict2=[{ "Produto":"CDB",
            "Taxa":0.82,
            "Aporte": 2000,
            "Prazo":360,
            "Periodicidade":30},
            
            { "Produto":"POUPANCA",
            "Taxa":0.92,
            "Aporte": 2000,
            "Prazo":360,
            "Periodicidade":30}]
            
            
    
                    
        

    

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 






def Inv_pontual(x):
    resposta=[]
    titulo=[]
    for produto in x:
        if produto["Produto"] in ["TESOURO-PRE", "TESOURO-SELIC"]:
            titulo.append(produto["Produto"]+ " "+ "Prazo:"+ str(produto["Prazo"])) #cria primeira linha da matriz com as legendas 
        else:
            titulo.append(produto["Produto"]+ " "+ str(produto["Taxa"]*100)+"%" + "  Prazo:"+ str(produto["Prazo"]))
    resposta.append(titulo)
    array=[]
    for produto in x:
        rent=Ponto_ponto(di_futuro_full[produto["Prazo"]-1][2] ,produto["Prazo"],produto["Aporte"],produto["Taxa"])#posicao 30 é referente ao valor 31
        if produto["Produto"]=="CDB":
            array.append(int(rent.cdb()))
        elif produto["Produto"]=="LCI"or produto["Produto"]=="LCA":
            array.append(int(rent.lci_lca()))
        elif produto["Produto"]=="POUPANCA":
            array.append(int(rent.poup()))
        elif produto["Produto"]=="TESOURO-PRE": 
            array.append(int(rent.Tesouro_pre()))
        elif produto["Produto"]=="TESOURO-SELIC": 
            array.append(int(rent.Tesouro_selic()))
        else: 
            array.append(0)
    resposta.append(array)
    return resposta
    
    
    
    
def Inv_continuo(x):
    resposta=[]
    titulo=[]
    for produto in user_dict2:
        if produto["Produto"]=="POUPANCA":
            titulo.append(produto["Produto"]+ " "+str(produto["Prazo"])+ " Dias" + " Aporte de " + str(produto["Aporte"]) + " a cada " + str(produto["Periodicidade"])) #cria primeira linha da matriz com as legendas 
        else:
            titulo.append(produto["Produto"]+" "+ str(produto["Taxa"]*100)+"% "+ str(produto["Prazo"])+ " Dias" + " Aporte de " + str(produto["Aporte"]) + " a cada " + str(produto["Periodicidade"])) #cria primeira linha da matriz com as legendas 
    resposta.append(titulo)
    array=[]
    for produto in user_dict2:
        rent=Aportes_periodicos(produto["Produto"],produto["Prazo"] ,produto["Periodicidade"],produto["Aporte"],produto["Taxa"])#posicao 30 é referente ao valor 31
        array.append(int(rent.looping()))
    resposta.append(array)
    return resposta
    
                 



print (Inv_pontual(user_dict))






















 

        
     
