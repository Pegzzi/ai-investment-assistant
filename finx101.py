# -*- coding: utf-8 -*-
"""
Created on Sun Jun 19 12:59:18 2016

@author: Fernando
"""
import re
  
######## FUCTIONS ###########################################     
def dict_tags(tagged_list):
    func_dict = {}    
    for member in tagged_list:
#        func_dict[member[1]].append(member[0])
        func_dict[member[1]] = member[0]

    return func_dict 

def tagger(sentence):
    tagger_list = []
    for word in sentence.split():
        if word in ['cdb', 'cdi', 'lci', 'lca', 'poupanca']:
            tagger_list.append((word, 'Produto'))
        elif re.compile('^[0-9]*%').match(word):
            tagger_list.append((word, 'Taxa'))
        elif re.compile('^[0-9]*$').match(word):
            tagger_list.append((word, 'Aporte'))
    return tagger_list
    
def action(sentence):
    rem = 0
    add = 0
    upd = 0
    for word in sentence.split():
        if word in ['remove', 'tira', 'retira', 'elimina',
                    'tirar', 'eliminar', 'retirar', 'deleta', 'deletar']:
            rem = rem + 1
        elif word in ['cola', 'coloca', 'colocar', 'bota', 'adiciona',
                      'compara', ' colocar', 'botar', 'calcula',
                      'simula', 'simular', 'adicionar']:
            add = add + 1
        elif word in ['troca', 'muda', 'modifica', 'trocar']:
            upd = upd + 1
            
    if rem+add+upd == 0: return 0
    if rem > add and rem > upd: return 2
    if add > rem and add > upd: return 1
    if upd > add and upd > rem: return 3

def make_active(func_dict):

    if 'Produto' in func_dict.keys():
        if 'Taxa' in func_dict.keys():
            if 'Aporte' in func_dict.keys():
                if 'Prazo' in func_dict.keys():
                    return 1
    return 0  
    
        
#checa se o dict consiste com dict necessario para alguma funcao    
def check_dict(func_dict):
    for key in func_dict.keys():   
        if not func_dict[key]:
            return 0
    return 1

    



def start_conversation(lista):    

    user_dashboard = []
    memory = []
   
    print('O que voce gostaria de simular? \n')     
    while True:
        if not lista:    
            sentence = input('>>')
            print('\n')
        else:
            sentence = lista.pop(0)
            print('>> ',sentence, '\n')
            
        memory.append(sentence)
        if sentence == 'quit': break 
        if sentence == 'dashboard':  print(user_dashboard)
            
        tagged_list = tagger(sentence)
    
    
        # atualizando variaveis atuais
        for tag in tagged_list:    
            if tag[1] == str('Produto'):
                produto = tag[0].upper()  
            elif tag[1] == str('Taxa'):
                taxa = float(tag[0].strip('%'))/100
            elif tag[1] == str('Aporte'):
                aporte = float(tag[0])
        prazo = 365        
                
    
        action_var = action(sentence)
    
    
        if action_var == 1: # default de adicionar produto
            print('Produto Adicionado')
            
            current_dict = {}
            current_dict['Produto'] = produto            
            current_dict['Taxa'] = taxa
            current_dict['Aporte'] = aporte
            current_dict['Prazo'] = prazo
            current_dict['Ativo'] = make_active(current_dict)       
            user_dashboard.append(current_dict)
            
        elif action_var == 2: # remover produto 
            print ('Deletando produto')
            
            for member in user_dashboard:
                if 'Produto' in member.keys():
                    if member['Produto'] == produto:
                        member['Ativo'] = 0
    
        elif action_var == 3: # update produto
            print('Produto Atualizado')
            
            for member in user_dashboard:
                if 'Produto' in member.keys():
                    if member['Produto'] == produto:
                        for tag in tagged_list:     
                            if tag[1] == str('Taxa'):
                                member['Taxa'] = float(tag[0].strip('%'))/100
                            elif tag[1] == str('Aporte'):
                                member['Aporte'] = float(tag[0])                    
                            elif tag[1] == str('Periodo'):
                                member['Periodo'] = tag[0] 
                member['Ativo'] = make_active(member)
            
        else:
            print('acao nao identificada')        
            
            
#        print('\nDicts Ativos:')    
#        for member in user_dashboard:
#            if member['Ativo'] == 1: print(member)
#        print('\nDicts Inativos:')
#        for member in user_dashboard:
#            if member['Ativo'] == 0: print(member)        
#       
#        print('\n')      
#        
        lista_invest = []
        for member in user_dashboard:
            if member['Ativo'] == 1:              
                lista_invest.append(member)
#        print(lista_invest)
#                
        print(lista_invest, '\n')
        print(Inv_pontual(lista_invest))



######################## MAIN ###############################


testes = [
#'Finx, calcula pra mim um cdb 82% com aporte de 1200',
#'compara com um lci 120%',
#'muda o cdb p/ 82%' ,
#'retira o cdb',
#'cola uma lca 85%',
#'quit'
]







start_conversation(testes)

     
    
        
#    
#    KR_questions = {'Produto':'Qual produto?', 
#                    'Taxa':'Qual taxa?', 
#                    'Aporte':'Qual aporte?',
#                    'Prazo': 'Qual prazo?',
#                    'Periodicidade':'Qual periodicidade?'}    
#    
    